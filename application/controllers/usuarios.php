<?php 
	class Usuarios extends CI_Controller {
		public function novo(){
			$post = $this->input->post();

			$nome = $post['nome'];
			$email = $post['email'];
			$senha = md5($post['senha']);
			
			$usuario = array(
				'nome' => $nome,
				'email' => $email,
				'senha' => $senha,
				);

			//$this->load->database(); Não precisamos mais carregar isso pois já foi setado no autoload.php do CI
			$this->load->model('usuarios_model');
			$this->usuarios_model->salva($usuario);

			$this->load->view('novo');
		}

 }

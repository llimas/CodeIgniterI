<?php 

class Login extends CI_Controller{
	
	public function autenticar(){

	
		$post = $this->input->post();
		$email = $post['email'];
		$senha = md5($post['senha']);

		$this->load->model('usuarios_model');

		$usuario = $this->usuarios_model->buscaPorEmailESenha($email, $senha);	

		if($usuario){
			$this->session->set_userdata('usuario', $usuario);
			$this->session->set_flashdata("success", "Logado com sucesso.");
			
		}else{
			$this->session->set_flashdata("danger", "Não foi possível logar.");
		}
		return redirect('/');
	}

	public function logout(){
		$this->session->unset_userdata('usuario');
		$this->session->set_flashdata("success", "Deslogado com sucesso.");
		return redirect('/');
	}
}
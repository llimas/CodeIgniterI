<?php 
	class Produtos extends CI_Controller {

		public function index(){

			//$this->load->database(); Não precisamos mais carregar isso pois já foi setado no autoload.php do CI
			
			$this->load->model('produtos_model');

			$this->load->helper(array('form', 'currency_helper'));
			
	        $produtos = $this->produtos_model->buscaTodos();
	        $dados = array("produtos" => $produtos);

	        $this->load->view("produtos/index.php" , $dados);
		}

		public function novo(){
			$userId = $this->session->userdata('usuario');
			
			$produto = array(
				'nome' => $this->input->post('nome'),
				'descricao' => $this->input->post('descricao'),
				'preco' => $this->input->post('preco'),
				'usuario_id' => $userId['id'],
				);
			
			$this->load->model('produtos_model');

			$this->produtos_model->salva($produto);

			$this->session->set_flashdata('success', 'Produto salvo com sucesso');

			return redirect('/');

		}

		public function formulario(){
			$this->load->view('produtos/novo');	
		}
	}
 ?>
<html lang="en">
   <head>
   	<link rel="stylesheet" type="text/css" href="<?= base_url("css/bootstrap.css") ?>">
   </head>
    <body>
        <div class="container">

        <?php if($this->session->flashdata("danger")): ?>
                <p class="alert alert-danger"><?= $this->session->flashdata("danger"); ?></p>
        <?php endif;
  
            if($this->session->flashdata("success")): ?>
            <p class="alert alert-success"><?= $this->session->flashdata("success"); ?></p>
       <?php endif; ?>
         
            <h1>Produtos</h1>
            	<table class="table">
                <th>Nome</th>
                <th>Descrição</th>
                <th>Preço</th>
            	<?php foreach ($produtos as $produto) : ?>
            		<tr>
            			<td><?= $produto['nome'] ?></td>
                        <td><?= $produto['descricao'] ?></td>
            			<td><?= numeroEmReais($produto['preco']) ?></td>
            		</tr>
            	<?php endforeach ?>
            	</table>
            
            
            <?php 
            if($this->session->userdata('usuario')):

                echo anchor('produtos/formulario', 'Adicionar Produto', array('class' => 'btn btn-primary'));
                echo anchor('login/logout', 'Logout', array('class' => 'btn btn-primary'));


            
            elseif(!$this->session->userdata('usuario')): ?>

            <h1>Login</h1>
            
            <?php
            echo form_open("login/autenticar");
            echo form_label("E-mail", "email");
            echo form_input(array(
                "name" => "email", 
                "id" => "email",
                "class" => "form-control",
                "maxlenght" => 255
                ));

            echo form_label("Senha", "senha");
            echo form_password(array(
                "name" => "senha",
                "id" => "senha",
                "class" => "form-control",
                "maxlength" => "255"
                ));
            echo form_button(array(
                "class" => "btn btn-primary",
                "content" => "Login",
                "type" => "submit"
                ));
            echo form_close();

            echo form_open("usuarios/novo");
            
            echo form_label("Nome", "nome");
            echo form_input(array(
                "name" => "nome",
                "maxlenght" => 255,
                "id" => "nome",
                "class" => "form-control"
                ));

            echo form_label("E-mail", "email");
            echo form_input(array(
                "name" => "email", 
                "id" => "email",
                "class" => "form-control",
                "maxlenght" => 255
                ));

            echo form_label("Senha", "senha");
            echo form_password(array(
                "name" => "senha",
                "id" => "senha",
                "class" => "form-control",
                "maxlength" => "255"
                ));

            echo form_button(array(
                "class" => "btn btn-primary",
                "content" => "Cadastrar",
                "type" => "submit"
                ));
            
            echo form_close();
            endif;
            ?>
       </div>
    </body>
</html>
